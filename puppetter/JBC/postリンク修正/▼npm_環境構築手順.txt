▼npm 環境構築手順

1.最初から構築する場合
�@ターミナルを開く
�A構築対象のフォルダへ移動する
�Bnpm init　コマンドを実行してpackage.jsonを作成する
�Cnpm install パッケージ名 --save
例：
npm install fs --save
npm install xlsx --save
npm install cheerio --save
npm install request --save

※参考
https://qiita.com/hashrock/items/15f4a4961183cfbb2658



2.package.jsonを元に構築する場合
※構築対象のフォルダへpackage.jsonを格納しておく。
�@ターミナルを開く
�A構築対象のフォルダへ移動する
�Bnpm install　コマンドを実行する
これで, package.json の dependencies, devDependencies に記述されている パッケージを一括でインストール

※参考
http://phiary.me/node-js-package-manager-npm-usage/#post-h2-id-2


▼js作成・実行手順
�@構築対象のフォルダへ任意のjsファイルを作成する（例：json_create.js）
�Ajsファイルへ処理を書く
�Bnode json_create.js　コマンドで実行