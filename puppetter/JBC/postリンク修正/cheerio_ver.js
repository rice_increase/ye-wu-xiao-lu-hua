const fs = require('fs')
const puppeteer = require('puppeteer')
const XLSX = require('xlsx')
const cheerio = require('cheerio')
// import XLSX, { utils } from 'xlsx'

const Utils = XLSX.utils;
const book_name = './src/リンク修正.xlsx'
const sheet_name = '整形ずみ'
const book = XLSX.readFile(book_name)
const sheet = book.Sheets[sheet_name]

let range = sheet['!ref']
const url_column = 'A'
const judge_column = 'B'
const action_val_column = 'C'
const start_row = 3
const end_row = Utils.decode_range(range).e.r + 1 //0からスタートなので1を足す

// const access = async function(url) {}
const access = async (url) => {
/**
 * ----- Warning -----
 * launch()とnewPage()をループで回すとブラウザが大量に起動されて処理落ちるするので
 * ループないで処理しない
 */

  const browser = await puppeteer.launch({headless: true,ignoreHTTPSErrors: true})
  const page = await browser.newPage()

  //エラーログ用
  var error_log = ''

  for(var i = start_row; i <= end_row; i++) {
    var url_cell = url_column + String(i)
    var url = (url_cell = { v:'' }) => url_cell
    
    if(url(sheet[url_cell]).v === '') continue

    //try,catchでエラーの際のダミーロードを用意
    //※書いていないとエラー起こしたタイミングで処理が止まる為 【参考】https://access-jp.co.jp/blogs/development/39
    try {
      // 遷移したいページURLを指定 第二引数のwaitUntilオプションでDOM読み込み完了を待たせる【参考】https://qiita.com/rh_taro/items/32bb6851303cbc613124
      await page.goto(url(sheet[url_cell]).v,{waitUntil: "domcontentloaded"})

      // 処理が終わるまでページの待機時間を設定
      await page.waitFor(5000)

      //テキストを取得する
      //【参考】https://techblog.gmo-ap.jp/2018/12/28/puppeteer%E3%81%A7%E3%81%A7%E3%81%8D%E3%82%8B%E3%81%93%E3%81%A8%E3%81%BE%E3%81%A8%E3%82%81/
      const bodyHandle = await page.$('body');
      const html = await page.evaluate(body => body.innerHTML, bodyHandle);
      const $ = await cheerio.load(html)
      
      // console.log('フォーム数:'+$('form[method="post"]').length)

      var judge_flg = 0;
      var action_val_data = '';
      $('form[method="post"]').each(function(){
        var action_val = String($(this).attr('action'))

        action_val_data += action_val + "\n"

        var result = action_val.indexOf('www.jal.co.jp');
        if(result !== -1) {
          judge_flg ++;
        }

        // console.log(action_val)
      });


      // xlsxファイルに書き込み
      //【参考】https://qiita.com/Kazunori-Kimura/items/29038632361fba69de5e

      //ページの種類
      var judge_data = '';
      if(judge_flg > 0){
        judge_data = 'あり';
      }else{
        judge_data = 'なし';
      }

      // console.log(judge_data)

      var judge_cell = judge_column + String(i)
      sheet[judge_cell] = { t: "s", v: judge_data, w: judge_data };

      var action_val_cell = action_val_column + String(i)
      sheet[action_val_cell] = { t: "s", v: action_val_data, w: action_val_data };

      // 範囲の更新を忘れずに!!
      sheet['!ref'];
      book.Sheets[sheet_name] = sheet;
      XLSX.writeFile(book, book_name);

      console.log(i+' : finished!!')
    }
    catch (e) {
      // エラー内容出力
      // console.log(e);
      console.log(i+' : error!!')
      error_log += url_cell + "\n"

      // エラーログをtxtファイルに書き込み
      await fs.writeFile(`./dist/error.txt`, error_log, err => {
        if(err) throw err
      })
    }
  }

  // エラーログをtxtファイルに書き込み
  // await fs.writeFile(`./dist/error.txt`, error_log, err => {
  //   if(err) throw err
  // })

  // 処理が完了したら必ずブラウザは閉じること
  await browser.close()
}
access()
