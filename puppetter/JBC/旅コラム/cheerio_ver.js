const fs = require('fs')
const puppeteer = require('puppeteer')
const XLSX = require('xlsx')
const cheerio = require('cheerio')
// import XLSX, { utils } from 'xlsx'

const Utils = XLSX.utils;
const book_name = './src/URLリスト.xlsx'
const sheet_name = 'URLリスト'
const book = XLSX.readFile(book_name)
const sheet = book.Sheets[sheet_name]

let range = sheet['!ref']
const url_column = 'B'
const pageType_column = 'C'
const ttl_column = 'D'
const date_column = 'E'
const writer_column = 'F'
const category_column = 'G'
const txt_column = 'H'
const start_row = 1394
const end_row = Utils.decode_range(range).e.r + 1 //0からスタートなので1を足す

// const access = async function(url) {}
const access = async (url) => {
/**
 * ----- Warning -----
 * launch()とnewPage()をループで回すとブラウザが大量に起動されて処理落ちるするので
 * ループないで処理しない
 */

  const browser = await puppeteer.launch({headless: true,ignoreHTTPSErrors: true})
  const page = await browser.newPage()

  //エラーログ用
  var error_log = ''

  for(var i = start_row; i <= end_row; i++) {
    var url_cell = url_column + String(i)
    var url = (url_cell = { v:'' }) => url_cell
    
    if(url(sheet[url_cell]).v === '') continue

    //try,catchでエラーの際のダミーロードを用意
    //※書いていないとエラー起こしたタイミングで処理が止まる為 【参考】https://access-jp.co.jp/blogs/development/39
    try {
      // 遷移したいページURLを指定 第二引数のwaitUntilオプションでDOM読み込み完了を待たせる【参考】https://qiita.com/rh_taro/items/32bb6851303cbc613124
      await page.goto(url(sheet[url_cell]).v,{waitUntil: "domcontentloaded"})

      // 処理が終わるまでページの待機時間を設定
      await page.waitFor(5000)

      //テキストを取得する
      //【参考】https://techblog.gmo-ap.jp/2018/12/28/puppeteer%E3%81%A7%E3%81%A7%E3%81%8D%E3%82%8B%E3%81%93%E3%81%A8%E3%81%BE%E3%81%A8%E3%82%81/
      const bodyHandle = await page.$('body');
      const html = await page.evaluate(body => body.innerHTML, bodyHandle);
      const $ = await cheerio.load(html)

      var ttl_data = await $('.articleTitTxt .title').text()
      var date_data = await $('.articleTitTxt p:first-child').text()
      var writer_data = await $('.articleTitTxt .writer').text()
      var category_data = await $('.articleTitTxt .category').text().replace('Category:　','')
      var txt_data = await $('.articleTxtArea').text()

      // xlsxファイルに書き込み
      //【参考】https://qiita.com/Kazunori-Kimura/items/29038632361fba69de5e

      //ページの種類
      var pageType_data = '';
      if($('.more').length){
        pageType_data = '一覧';
      }else{
        if($('.articleTitTxt').length){
          pageType_data = '詳細';
        }else{
          pageType_data = '一覧';
        }
      }

      var pageType_cell = pageType_column + String(i)
      sheet[pageType_cell] = { t: "s", v: pageType_data, w: pageType_data };

      if(pageType_data == '詳細'){
        //タイトル
        var ttl_cell = ttl_column + String(i)
        sheet[ttl_cell] = { t: "s", v: ttl_data, w: ttl_data };

        //日付
        var date_cell = date_column + String(i)
        sheet[date_cell] = { t: "s", v: date_data, w: date_data };

        //著者
        var writer_cell = writer_column + String(i)
        sheet[writer_cell] = { t: "s", v: writer_data, w: writer_data };

        //カテゴリー
        var category_cell = category_column + String(i)
        sheet[category_cell] = { t: "s", v: category_data, w: category_data };

        //テキスト
        var txt_cell = txt_column + String(i)
        sheet[txt_cell] = { t: "s", v: txt_data, w: txt_data };
      }

      // 範囲の更新を忘れずに!!
      sheet['!ref'];
      book.Sheets[sheet_name] = sheet;
      XLSX.writeFile(book, book_name);

      console.log(i-1+' : finished!!')
    }
    catch (e) {
      // エラー内容出力
      // console.log(e);
      console.log(i-1+' : error!!')
      error_log += url_cell + "\n"

      // エラーログをtxtファイルに書き込み
      await fs.writeFile(`./dist/error.txt`, error_log, err => {
        if(err) throw err
      })
    }
  }

  // エラーログをtxtファイルに書き込み
  // await fs.writeFile(`./dist/error.txt`, error_log, err => {
  //   if(err) throw err
  // })

  // 処理が完了したら必ずブラウザは閉じること
  await browser.close()
}
access()

