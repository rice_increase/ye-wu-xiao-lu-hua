const fs = require('fs')
const puppeteer = require('puppeteer')
const XLSX = require('xlsx')
// import XLSX, { utils } from 'xlsx'

const Utils = XLSX.utils;
const book = XLSX.readFile('./src/URLリスト.xlsx')
const sheet1 = book.Sheets['URLリスト']

let range = sheet1['!ref']
const base_column = 'C'
const start_row = 2
const end_row = Utils.decode_range(range).e.r + 1 //0からスタートなので1を足す

// const access = async function(url) {}

const access = async (url) => {
/**
 * ----- Warning -----
 * launch()とnewPage()をループで回すとブラウザが大量に起動されて処理落ちるするので
 * ループないで処理しない
 */

  const browser = await puppeteer.launch({headless: false,ignoreHTTPSErrors: true})
  const page = await browser.newPage()

  for(var i = start_row; i <= end_row; i++) {
    var cell = base_column + String(i)
    var url = (cell = { v:'' }) => cell
    
    if(url(sheet1[cell]).v === '') continue

    //try,catchでエラーの際のダミーロードを用意
    //※書いていないとエラー起こしたタイミングで処理が止まる為 【参考】https://access-jp.co.jp/blogs/development/39
    try {
      // 遷移したいページURLを指定 第二引数のwaitUntilオプションでDOM読み込み完了を待たせる【参考】https://qiita.com/rh_taro/items/32bb6851303cbc613124
      await page.goto(url(sheet1[cell]).v,{waitUntil: "domcontentloaded"})

      //テキストを取得する
      var data = await page.$eval('.articleTxtArea', item => {
        return item.textContent;
      });

      // 処理が終わるまでページの待機時間を設定
      await page.waitFor(3000)

      // txtファイルに書き込み
      await fs.writeFile(`./dist/${i}.txt`, data, err => {
        if(err) throw err
      })
    }
    catch (e) {
      // エラー内容出力
      console.log(e);

      // ブラウザを閉じて終了
      // await browser.close();
      // process.exit(200);
    }

  }
  // 処理が完了したら必ずブラウザは閉じること
  await browser.close()
}
access()

