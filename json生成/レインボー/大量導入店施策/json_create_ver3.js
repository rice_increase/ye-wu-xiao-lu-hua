const xlsx = require('xlsx');
const fs = require('fs');
const utils = xlsx.utils;

let workbook = xlsx.readFile('./src/xlsx/data.xlsx', {cellStyles:true,cellDates: true});//ファイル読み込み
let worksheet = workbook.Sheets['Sheet1'];//シートにアクセス

let range = worksheet['!ref'];//シートにアクセスの中のセルを対象とする
let rangeVal = utils.decode_range(range);//セル範囲を数値表現に変換

//配列作成
var json_data = [];
var shop_data = [];

var prefectures = ["北海道", "青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県", "茨城県", "栃木県", "群馬県", "埼玉県", "千葉県", "東京都", "神奈川県", "新潟県", "富山県", "石川県", "福井県", "山梨県", "長野県", "岐阜県", "静岡県", "愛知県", "三重県", "滋賀県", "京都府", "大阪府", "兵庫県", "奈良県", "和歌山県", "鳥取県", "島根県", "岡山県", "広島県", "山口県", "徳島県", "香川県", "愛媛県", "高知県","福岡県", "佐賀県", "長崎県", "熊本県", "大分県", "宮崎県", "鹿児島県", "沖縄県"];

for(let i=0; i<prefectures.length; i++ ){
    json_data.push({});
    json_data[json_data.length-1]['area'] = prefectures[i];
    json_data[json_data.length-1]['data'] = [];
}

//日付のフォーマット変更
function datetostr(date, format, is12hours) {
    var weekday = ["日", "月", "火", "水", "木", "金", "土"];
    if (!format) {
        format = 'YYYY/MM/DD(WW) hh:mm:dd'
    }
    var year = date.getFullYear();
    var month = (date.getMonth() + 1);
    var day = date.getDate();
    var weekday = weekday[date.getDay()];
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secounds = date.getSeconds();

    var ampm = hours < 12 ? 'AM' : 'PM';
    if (is12hours) {
        hours = hours % 12;
        hours = (hours != 0) ? hours : 12; // 0時は12時と表示する
    }

    var replaceStrArray =
        {
            'YYYY': year,
            'Y': year,
            'MM': ('0' + (month)).slice(-2),
            'M': month,
            'DD': ('0' + (day)).slice(-2),
            'D': day,
            'WW': weekday,
            'hh': ('0' + hours).slice(-2),
            'h': hours,
            'mm': ('0' + minutes).slice(-2),
            'm': minutes,
            'ss': ('0' + secounds).slice(-2),
            's': secounds,
            'AP': ampm,
        };

    var replaceStr = '(' + Object.keys(replaceStrArray).join('|') + ')';
    var regex = new RegExp(replaceStr, 'g');

    ret = format.replace(regex, function (str) {
        return replaceStrArray[str];
    });

    return ret;
}
// 使用方法
// console.log(datetostr(new Date(), 'Y/MM/DD(WW) hh:mm:ss AP', false));

//エクセルからデータ取得
//A列→0, B列→1, C列→2, D列→3, E列→4
//1行目→0, 2行目→1, 3行目→2, 4行目→3, 5行目→4
for (let C=3; C <= 3; C++){//列 D列回す
    for (let R=3; R <= rangeVal.e.r; R++){//行 4行目から回す
        let adr = utils.encode_cell({c:C, r:R});//c:セル番号(A=0) r:行番号(1=0)
        let cell = worksheet[adr];
        if(cell !== undefined){//セルが空白でなけれれば
            var link_id = String(cell.v);
            var hall_name = worksheet[utils.encode_cell({c:5, r:R})] !== undefined? String(worksheet[utils.encode_cell({c:5, r:R})].v) : '';
            var address = worksheet[utils.encode_cell({c:6, r:R})] !== undefined? String(worksheet[utils.encode_cell({c:6, r:R})].v) : '';

            var area = "";
            var prefectures = ["北海道", "青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県", "茨城県", "栃木県", "群馬 ", "埼玉県", "千葉県", "東京都", "神奈川県", "新潟県", "富山県", "石川県", "福井県", "山梨県", "長野県", "岐阜県", "静岡県", "愛知県", "三重県", "滋賀県", "京都府", "大阪府", "兵庫県", "奈良県", "和歌山県", "鳥取県", "島根県", "岡山県", "広島県", "山口県", "徳島県", "香川県", "愛媛県", "高知県","福岡県", "佐賀県", "長崎県", "熊本県", "大分県", "宮崎県", "鹿児島県", "沖縄県"];

            for(let i=0; i<prefectures.length; i++ ){
                if(address.match(prefectures[i])){
                    var area = prefectures[i];
                }
            }

            // var page_type =  worksheet[utils.encode_cell({c:8, r:R})] !== undefined? String(worksheet[utils.encode_cell({c:8, r:R})].v) : '';
            var url = worksheet[utils.encode_cell({c:9, r:R})] !== undefined? String(worksheet[utils.encode_cell({c:9, r:R})].v) : '';
            var release_date_get = worksheet[utils.encode_cell({c:10, r:R})] !== undefined? worksheet[utils.encode_cell({c:10, r:R})].v : '';
            var release_date = datetostr(release_date_get, 'Y/MM/DD', false);

            //Json側(shop_data)へデータを挿入
            shop_data.push({});
            shop_data[shop_data.length-1]['id'] = link_id; //7Link_ID
            shop_data[shop_data.length-1]['hall_name'] = hall_name; //ホール名称
            shop_data[shop_data.length-1]['address'] = address; //住所
            shop_data[shop_data.length-1]['area'] = area; //地域【都道府県】
            // shop_data[shop_data.length-1]['page_type'] = page_type; //ページの種類
            shop_data[shop_data.length-1]['url'] = url; //リンク先
            shop_data[shop_data.length-1]['release_date'] = release_date; //公開日
        }
    }
}

for(let j=0; j<shop_data.length; j++){
    var shop_area = shop_data[j]['area'];
    for(let k=0; k<json_data.length; k++){
        if(json_data[k]['area'] == shop_area){

            //Json側(json_data)へデータを挿入
            json_data[k]['data'].push({});
            json_data[k]['data'][json_data[k]['data'].length-1]['id'] = shop_data[j]['id'];
            json_data[k]['data'][json_data[k]['data'].length-1]['hall_name'] = shop_data[j]['hall_name'];
            json_data[k]['data'][json_data[k]['data'].length-1]['address'] = shop_data[j]['address'];
            // json_data[k]['data'][json_data[k]['data'].length-1]['page_type'] = shop_data[j]['page_type'];
            json_data[k]['data'][json_data[k]['data'].length-1]['url'] = shop_data[j]['url'];
            json_data[k]['data'][json_data[k]['data'].length-1]['release_date'] = shop_data[j]['release_date'];
        }
    }
}

//空の配列削除
for(let l=0; l<json_data.length; l++){
    if(json_data[l]['data'].length == 0){
        delete json_data[l];
    }
}
var removeObject = json_data.filter(v => v);

//jsonへ書き出し
var json_conversion = JSON.stringify(removeObject,null,2);//jsonファイルの整形

fs.writeFile('./src/json/shop.json', json_conversion, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
});

console.log(shop_data.length+'店舗の情報を出力しました');