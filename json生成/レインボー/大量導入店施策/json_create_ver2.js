const xlsx = require('xlsx');
const fs = require('fs');
const utils = xlsx.utils;

let workbook = xlsx.readFile('./src/xlsx/data.xlsx', {cellStyles:true,cellDates: true});//ファイル読み込み
let worksheet = workbook.Sheets['Sheet1'];//シートにアクセス

let range = worksheet['!ref'];//シートにアクセスの中のセルを対象とする
let rangeVal = utils.decode_range(range);//セル範囲を数値表現に変換

//配列作成
var json_data = [];
var shop_data = [];

var prefectures = ["北海道", "青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県", "茨城県", "栃木県", "群馬県", "埼玉県", "千葉県", "東京都", "神奈川県", "新潟県", "富山県", "石川県", "福井県", "山梨県", "長野県", "岐阜県", "静岡県", "愛知県", "三重県", "滋賀県", "京都府", "大阪府", "兵庫県", "奈良県", "和歌山県", "鳥取県", "島根県", "岡山県", "広島県", "山口県", "徳島県", "香川県", "愛媛県", "高知県","福岡県", "佐賀県", "長崎県", "熊本県", "大分県", "宮崎県", "鹿児島県", "沖縄県"];

for(let i=0; i<prefectures.length; i++ ){
    json_data.push({});
    json_data[json_data.length-1]['area'] = prefectures[i];
    json_data[json_data.length-1]['data'] = [];
}

//エクセルからデータ取得
//A列→0, B列→1, C列→2, D列→3, E列→4
//1行目→0, 2行目→1, 3行目→2, 4行目→3, 5行目→4
for (let C=3; C <= 3; C++){//列 D列回す
    for (let R=3; R <= rangeVal.e.r; R++){//行 4行目から回す
        let adr = utils.encode_cell({c:C, r:R});//c:セル番号(A=0) r:行番号(1=0)
        let cell = worksheet[adr];
        if(cell !== undefined){//セルが空白でなけれれば
            var link_id = String(cell.v);
            var hall_name = worksheet[utils.encode_cell({c:5, r:R})] !== undefined? String(worksheet[utils.encode_cell({c:5, r:R})].v) : '';
            var hall_name = worksheet[utils.encode_cell({c:5, r:R})] !== undefined? String(worksheet[utils.encode_cell({c:5, r:R})].v) : '';
            var address = worksheet[utils.encode_cell({c:6, r:R})] !== undefined? String(worksheet[utils.encode_cell({c:6, r:R})].v) : '';

            var area = "";
            var prefectures = ["北海道", "青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県", "茨城県", "栃木県", "群馬 ", "埼玉県", "千葉県", "東京都", "神奈川県", "新潟県", "富山県", "石川県", "福井県", "山梨県", "長野県", "岐阜県", "静岡県", "愛知県", "三重県", "滋賀県", "京都府", "大阪府", "兵庫県", "奈良県", "和歌山県", "鳥取県", "島根県", "岡山県", "広島県", "山口県", "徳島県", "香川県", "愛媛県", "高知県","福岡県", "佐賀県", "長崎県", "熊本県", "大分県", "宮崎県", "鹿児島県", "沖縄県"];

            for(let i=0; i<prefectures.length; i++ ){
                if(address.match(prefectures[i])){
                    var area = prefectures[i];
                }
            }

            var page_type =  worksheet[utils.encode_cell({c:8, r:R})] !== undefined? String(worksheet[utils.encode_cell({c:8, r:R})].v) : '';
            var url = worksheet[utils.encode_cell({c:9, r:R})] !== undefined? String(worksheet[utils.encode_cell({c:9, r:R})].v) : '';

            //Json側(shop_data)へデータを挿入
            shop_data.push({});
            shop_data[shop_data.length-1]['id'] = link_id; //7Link_ID
            shop_data[shop_data.length-1]['hall_name'] = hall_name; //ホール名称
            shop_data[shop_data.length-1]['address'] = address;
            shop_data[shop_data.length-1]['area'] = area;
            shop_data[shop_data.length-1]['page_type'] = page_type;
            shop_data[shop_data.length-1]['url'] = url;
        }
    }
}

for(let j=0; j<shop_data.length; j++){
    var shop_area = shop_data[j]['area'];
    for(let k=0; k<json_data.length; k++){
        if(json_data[k]['area'] == shop_area){

            //Json側(json_data)へデータを挿入
            json_data[k]['data'].push({});
            json_data[k]['data'][json_data[k]['data'].length-1]['id'] = shop_data[j]['id'];
            json_data[k]['data'][json_data[k]['data'].length-1]['hall_name'] = shop_data[j]['hall_name'];
            json_data[k]['data'][json_data[k]['data'].length-1]['address'] = shop_data[j]['address'];
            json_data[k]['data'][json_data[k]['data'].length-1]['page_type'] = shop_data[j]['page_type'];
            json_data[k]['data'][json_data[k]['data'].length-1]['url'] = shop_data[j]['url'];
        }
    }
}

//空の配列削除
for(let l=0; l<json_data.length; l++){
    if(json_data[l]['data'].length == 0){
        delete json_data[l];
    }
}
var removeObject = json_data.filter(v => v);

//jsonへ書き出し
var json_conversion = JSON.stringify(removeObject,null,2);//jsonファイルの整形

fs.writeFile('./src/json/shop.json', json_conversion, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
});

console.log(shop_data.length+'店舗の情報を出力しました');