const xlsx = require('xlsx');
const fs = require('fs');
const utils = xlsx.utils;

let workbook = xlsx.readFile('./src/xlsx/記事リスト_282記事.xlsx', {cellStyles:true,cellDates: true});//ファイル読み込み
let worksheet = workbook.Sheets['URLリスト_書き出し用'];//シートにアクセス

let range = worksheet['!ref'];//シートにアクセスの中のセルを対象とする
let rangeVal = utils.decode_range(range);//セル範囲を数値表現に変換

//let json_file = require('./src/json/magazine.json');

//配列作成
//var json_data = [].concat(json_file);
var json_data = [];


//エクセルからデータ取得
for (let C=4; C <= 4; C++){//列 E列回す
    for (let R=0; R <= rangeVal.e.r; R++){//行 1行目から回す
        let adr = utils.encode_cell({c:C, r:R});//c:セル番号(A=0) r:行番号(1=0)
        let cell = worksheet[adr];
        if(cell !== undefined){
            var cell_data = String(cell.v).split('_');
            var cate = cell_data[0];
            var id = cell_data[1];
            var title = worksheet[utils.encode_cell({c:1, r:R})] !== undefined ? String(worksheet[utils.encode_cell({c:1, r:R})].v).replace(/\r\n/g,'').replace(/\n|\r/g, '') : '';
            var desc = worksheet[utils.encode_cell({c:2, r:R})] !== undefined ? String(worksheet[utils.encode_cell({c:2, r:R})].v).replace(/\r\n/g,'').replace(/\n|\r/g, '') : '';
            var related = [];
            var judg_arry = String(worksheet[utils.encode_cell({c:0, r:R})].v).split('/');
            var judg_cate = judg_arry[judg_arry.length-2];
            var judg_id = judg_arry[judg_arry.length-1].replace('.html','');

            //ページのディレクトリとカテゴリが一致しない場合
            if(judg_cate != String(cate)){
                console.log('カテゴリに異常有り：'+cell.v+'のアイテムです');
            }

            //ページのナンバーとIDが一致しない場合
            if(judg_id != String(id)){
                console.log('IDに異常有り：'+cell.v+'のアイテムです');
            }

            for (let i=0; i <=2; i++){//列 F~H列回す
                var related_cell = worksheet[utils.encode_cell({c:5+i, r:R})];
                if(related_cell !== undefined){
                    if(related_cell.s !== undefined){
                        if(related_cell.s.fgColor !== undefined){
                            if(related_cell.s.fgColor.rgb !== undefined || related_cell.s.fgColor.tint !== undefined){//セルが塗りつぶされているか判定
                                related.push('');//セルが塗りつぶされていれば
                            }
                        }else{
                            related.push(String(worksheet[utils.encode_cell({c:5+i, r:R})].v));//セルが塗りつぶされていなければ
                        }
                    }else{
                        related.push(String(worksheet[utils.encode_cell({c:5+i, r:R})].v));//セルが塗りつぶされていなければ
                    }
                }else{
                    related.push('');//セルの中身が無い場合
                }
            }

            //テーマリンク重複確認
            var check_ary = [].concat(related);
            var jyuhuku_count = 0;
            for(let k = 0; k<related.length; k++){
              var flag = 0;
              if(related[k] != ''){
                for(let l = 0; l<check_ary.length; l++){
                    if(related[k] == check_ary[l]){
                        flag++;
                        if(flag >= 2){
                          jyuhuku_count++;
                          console.log(cate+id+'でテーマリンクの重複が確認されました: '+related);
                        }
                    }
                }
              }
            }

            //Json側へデータを挿入
            var data_none_flg = 0;
            if(json_data.length == 0){
                json_data.push({});
                json_data[json_data.length-1]['categ'] = cate;
                json_data[json_data.length-1]['data'] = [];
                json_data[json_data.length-1]['data'].push({});
                json_data[json_data.length-1]['data'][0]['id'] = id;
                json_data[json_data.length-1]['data'][0]['title'] = title;
                json_data[json_data.length-1]['data'][0]['desc'] = desc;
                json_data[json_data.length-1]['data'][0]['related'] = related;
            }

            for(let j=0; j<json_data.length; j++){
                if(json_data[j]['categ'] == cate){//カテゴリが見つかった場合
                    var flg = 0;
                    var flg_num;
                    for(let j_d =0; j_d <json_data[j]['data'].length; j_d++){
                        if(json_data[j]['data'][j_d]['id'] == id){
                            flg++;
                            flg_num = j_d;
                        }
                    }
                    if(1 <= flg){//同じID番号のものがあった場合 データ上書き
                        json_data[j]['data'][flg_num]['id'] = id;
                        json_data[j]['data'][flg_num]['title'] = title;
                        json_data[j]['data'][flg_num]['desc'] = desc;
                        json_data[j]['data'][flg_num]['related'] = related;
                    }else{//なければ配列の末端へデータ追加
                        json_data[j]['data'].push({});
                        json_data[j]['data'][json_data[j]['data'].length-1]['id'] = id;
                        json_data[j]['data'][json_data[j]['data'].length-1]['title'] = title;
                        json_data[j]['data'][json_data[j]['data'].length-1]['desc'] = desc;
                        json_data[j]['data'][json_data[j]['data'].length-1]['related'] = related;
                    }
                }else{
                    data_none_flg++;
                    if(data_none_flg == json_data.length){//カテゴリが見つかった場合
                        json_data.push({});
                        json_data[json_data.length-1]['categ'] = cate;
                        json_data[json_data.length-1]['data'] = [];
                        json_data[json_data.length-1]['data'].push({});
                        json_data[json_data.length-1]['data'][0]['id'] = id;
                        json_data[json_data.length-1]['data'][0]['title'] = title;
                        json_data[json_data.length-1]['data'][0]['desc'] = desc;
                        json_data[json_data.length-1]['data'][0]['related'] = related;
                    }
                }
            }
        }
    }
}

//Jsonのデータをソートする
for(let s=0; s<json_data.length; s++){
    json_data[s]['data'].sort(function(a,b){
        if(a.id<b.id) return -1;
        if(a.id > b.id) return 1;
        return 0;
    });
}

var len_total = 0;
for(let l=0; l<json_data.length; l++){
    var len = json_data[l]['data'].length;
    len_total = parseInt(len_total) + parseInt(len);
}
console.log(len_total+'記事が書き出されました。');

//jsonへ書き出し
var json_conversion = JSON.stringify(json_data,null,2);//jsonファイルの整形

fs.writeFile('./src/json/result_282記事.json', json_conversion, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
});
